<?php

declare (strict_types=1);

namespace Drupal\smsapi;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of SMSAPI SMS Templates.
 */
class SmsapiSmsTemplateListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Label'),
      'id' => $this->t('Machine name'),
      'description' => $this->t('Description'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\smsapi\SmsapiSmsTemplateInterface $entity */
    return [
      'label' => $entity->label(),
      'id' => $entity->id(),
      'description' => $entity->get('description') ?? '',
    ] + parent::buildRow($entity);
  }

}
