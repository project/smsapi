<?php

declare(strict_types=1);

namespace Drupal\smsapi\Services;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\smsapi\SmsapiSmsTemplateInterface;

/**
 * SMSAPI SMS Template Service.
 */
class SmsapiSmsTemplateService implements SmsapiSmsTemplateServiceInterface {

  /**
   * The SMSAPI SMS Template entity storage.
   */
  protected EntityStorageInterface $smsapiSmsTemplateStorage;

  /**
   * Constructs a new SmsapiSmsTemplateService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager) {
    $this->smsapiSmsTemplateStorage = $this->entityTypeManager->getStorage('smsapi_sms_template');
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate(string $template_name): ?SmsapiSmsTemplateInterface {
    /** @var \Drupal\smsapi\SmsapiSmsTemplateInterface|null */
    return $this->smsapiSmsTemplateStorage->load($template_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplates(): ?array {
    return $this->smsapiSmsTemplateStorage->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function renderTemplate(SmsapiSmsTemplateInterface $template, array $values): string {
    $message = $template->getMessage();
    foreach ($template->getTokens() as $token) {
      $message = str_replace($token, $values[$token], $message);
    }
    return $message;
  }

}
