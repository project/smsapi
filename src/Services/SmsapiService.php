<?php

declare(strict_types=1);

namespace Drupal\smsapi\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Smsapi\Client\Curl\SmsapiHttpClient;
use Smsapi\Client\Feature\Mfa\Bag\CreateMfaBag;
use Smsapi\Client\Feature\Mfa\Bag\VerificationMfaBag;
use Smsapi\Client\Feature\Mfa\Data\Mfa;
use Smsapi\Client\Feature\Profile\Data\Profile;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;
use Smsapi\Client\Feature\Sms\Bag\SendSmssBag;
use Smsapi\Client\Feature\Sms\Data\Sms;
use Smsapi\Client\Infrastructure\ResponseMapper\ApiErrorException;
use Smsapi\Client\SmsapiClient;
use Symfony\Component\HttpFoundation\Request;

/**
 * SMSAPI Service.
 */
class SmsapiService implements SmsapiServiceInterface {
  use StringTranslationTrait;

  /**
   * SMSAPI config machine name.
   */
  private const SMSAPI_CONFIG = 'smsapi.settings';

  /**
   * SMSAPI dev environment.
   */
  private const SMSAPI_ENVIRONMENT_STATUS_DEV = 'DEV';

  /**
   * SMSAPI prod environment.
   */
  private const SMSAPI_ENVIRONMENT_STATUS_PROD = 'PROD';

  /**
   * SMSAPI sms encoding.
   */
  private const SMSAPI_ENCODING = 'utf-8';

  /**
   * SMSAPI active sender status.
   */
  private const SMSAPI_ACTIVE_SENDER = 'ACTIVE';

  /**
   * SMSAPI active sender status.
   */
  private const SMSAPI_MFA_URL = 'https://api.smsapi.pl/mfa/codes/verifications';

  /**
   * The SMSAPI token.
   */
  private string $apiToken;

  /**
   * The SMSAPI Partner Status.
   */
  private bool $partnerStatus;

  /**
   * The SMSAPI Partner ID.
   */
  private string $partnerId;

  /**
   * The SMSAPI service environment.
   *
   * Possible values: 'PROD' or 'DEV'.
   * If 'DEV' the SMS will be sent to the configured phone number.
   * If 'PROD' the SMS will be sent to the phone number from the form.
   */
  private string $serviceEnvironment;

  /**
   * The SMSAPI sending mock mode.
   *
   * If TRUE, the SMS will not be sent.
   */
  private bool $mockMode;

  /**
   * The SMSAPI status message visibility.
   */
  private bool $statusMessage;

  /**
   * The logger channel.
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a SMSAPI Service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config form variables.
   * @param \Smsapi\Client\SmsapiClient $client
   *   The config form variables.
   * @param \Smsapi\Client\Feature\Mfa\Bag\CreateMfaBag $mfaBag
   *   The mfa verification bag.
   * @param \Drupal\Core\Messenger\MessengerInterface $drupalMessenger
   *   The Drupal Messenger service.
   * @param \Drupal\smsapi\Services\SmsapiSmsTemplateServiceInterface $smsapiSmsTemplateService
   *   The SMSAPI SMS Template service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory service.
   */
  public function __construct(
    private ConfigFactoryInterface $config,
    private SmsapiClient $client,
    private CreateMfaBag $mfaBag,
    private MessengerInterface $drupalMessenger,
    private SmsapiSmsTemplateServiceInterface $smsapiSmsTemplateService,
    LoggerChannelFactoryInterface $logger_channel_factory,
  ) {
    $this->logger = $logger_channel_factory->get('smsapi');

    // Load SMSAPI token.
    $tokenValue = $this->config->get(self::SMSAPI_CONFIG)->get('smsapi_token');
    $this->apiToken = is_string($tokenValue) ? $tokenValue : '';

    // Load SMSAPI Partner info.
    $partnerStatus = $this->config->get(self::SMSAPI_CONFIG)->get('smsapi_partner');
    $partnerValue = $this->config->get(self::SMSAPI_CONFIG)->get('smsapi_partner_id');
    $this->partnerStatus = is_bool($partnerStatus) ? $partnerStatus : FALSE;
    $this->partnerId = is_string($partnerValue) ? $partnerValue : '';

    if (empty($this->apiToken)) {
      $this->logger->error('SMSAPI token is empty. Check the configuration.');
    }

    $this->serviceEnvironment = (bool) $this->config->get(self::SMSAPI_CONFIG)
      ->get('smsapi_environment_status') ? self::SMSAPI_ENVIRONMENT_STATUS_DEV : self::SMSAPI_ENVIRONMENT_STATUS_PROD;

    $this->mockMode = boolval($this->config->get(self::SMSAPI_CONFIG)
      ->get('smsapi_mock_mode') ?? FALSE);

    $this->statusMessage = boolval($this->config->get(self::SMSAPI_CONFIG)
      ->get('smsapi_status_message') ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function checkIfConnected(?string $api_token = ''): bool {
    $service = $this->client->smsapiPlService(!empty($api_token) ? $api_token : $this->apiToken);
    $result = $service->pingFeature()->ping();
    return $result->authorized;
  }

  /**
   * {@inheritdoc}
   */
  public function sendSms(string $phone_number, string $message, string $sender = ''): ?Sms {
    if ($this->serviceEnvironment == self::SMSAPI_ENVIRONMENT_STATUS_DEV) {
      $phone_number = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_phone_number');
      $phone_number = is_string($phone_number) ? $phone_number : '';

      if (empty($phone_number)) {
        if ($this->statusMessage) {
          $this->drupalMessenger->addWarning($this->t('Test phone number is not set. Please set it in the SMSAPI settings.'));
        }
        $this->logger->warning('SMSAPI: Test phone number is not set. Please set it in the SMSAPI settings.');
        return NULL;
      }
      $sender = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_sender_name');
      $sender = is_string($sender) ? $sender : '';
    }
    $service = $this->client->smsapiPlService($this->apiToken);
    $sms = SendSmsBag::withMessage($phone_number, $message);
    $sms->encoding = self::SMSAPI_ENCODING;
    if (!empty($sender)) {
      $sms->from = $sender;
    }
    if ($this->mockMode) {
      $sms->test = TRUE;
    }
    if ($this->partnerStatus) {
      $sms->partnerId = $this->partnerId;
    }

    try {
      $response = $service->smsFeature()->sendSms($sms);
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Message sent'), 'status');
      }
      return $response;
    }
    catch (\Exception $e) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Failed to send message'), 'error');
      }
      $this->logger->error('SMSAPI: Error sending message @error', [
        '@error' => $e->getMessage(),
      ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendSmsWithTemplate(
      string $phone_number,
      string $template_name,
      array $values,
      string $sender = '',
  ): ?Sms {
    if ($this->serviceEnvironment == self::SMSAPI_ENVIRONMENT_STATUS_DEV) {
      $phone_number = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_phone_number');
      $phone_number = is_string($phone_number) ? $phone_number : '';

      if (empty($phone_number)) {
        if ($this->statusMessage) {
          $this->drupalMessenger->addWarning($this->t('Test phone number is not set. Please set it in the SMSAPI settings.'));
        }
        $this->logger->warning('SMSAPI: Test phone number is not set. Please set it in the SMSAPI settings.');
        return NULL;
      }
      $sender = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_sender_name');
      $sender = is_string($sender) ? $sender : '';
    }
    $template = $this->smsapiSmsTemplateService->getTemplate($template_name);
    if ($template === NULL) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addWarning($this->t('Template @template_name not found.', [
          '@template_name' => $template_name,
        ]));
      }
      return NULL;
    }
    $message = $this->smsapiSmsTemplateService->renderTemplate($template, $values);
    $service = $this->client->smsapiPlService($this->apiToken);
    $sms = SendSmsBag::withMessage($phone_number, $message);
    $sms->encoding = self::SMSAPI_ENCODING;
    if (!empty($sender)) {
      $sms->from = $sender;
    }
    if ($this->mockMode) {
      $sms->test = TRUE;
    }
    if ($this->partnerStatus) {
      $sms->partnerId = $this->partnerId;
    }

    try {
      $response = $service->smsFeature()->sendSms($sms);
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Message sent'), 'status');
      }
      return $response;
    }
    catch (\Exception $e) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Failed to send message'), 'error');
      }
      $this->logger->error('SMSAPI: Error sending message @error', [
        '@error' => $e->getMessage(),
      ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendMultipleTemplateSms(
      array $phone_numbers,
      string $template_name,
      array $values,
      string $sender = '',
  ): ?array {
    if ($this->serviceEnvironment === self::SMSAPI_ENVIRONMENT_STATUS_DEV) {
      $phone_numbers = [];

      $test_phone_number = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_phone_number');
      if (empty($test_phone_number)) {
        if ($this->statusMessage) {
          $this->drupalMessenger->addWarning($this->t('Test phone number is not set. Please set it in the SMSAPI settings.'));
        }
        $this->logger->warning('SMSAPI: Test phone number is not set. Please set it in the SMSAPI settings.');
        return NULL;
      }
      $sender = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_sender_name');
      $sender = is_string($sender) ? $sender : '';
      $phone_numbers[] = $test_phone_number;
    }
    $template = $this->smsapiSmsTemplateService->getTemplate($template_name);
    if ($template === NULL) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addWarning($this->t('Template @template_name not found.', [
          '@template_name' => $template_name,
        ]));
      }
      return NULL;
    }
    $message = $this->smsapiSmsTemplateService->renderTemplate($template, $values);
    $service = $this->client->smsapiPlService($this->apiToken);
    $sms = SendSmssBag::withMessage($phone_numbers, $message);
    $sms->encoding = self::SMSAPI_ENCODING;
    if (!empty($sender)) {
      $sms->from = $sender;
    }
    if ($this->mockMode) {
      $sms->test = TRUE;
    }
    if ($this->partnerStatus) {
      $sms->partnerId = $this->partnerId;
    }

    try {
      $response = $service->smsFeature()->sendSmss($sms);
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Message sent'), 'status');
      }
      return $response;
    }
    catch (\Exception $e) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Failed to send multiple messages'), 'error');
      }
      $this->logger->error('SMSAPI: Error sending multiple messages @error', [
        '@error' => $e->getMessage(),
      ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendMultipleSms(array $phone_numbers, string $message, string $sender = ''): ?array {
    if ($this->serviceEnvironment === self::SMSAPI_ENVIRONMENT_STATUS_DEV) {
      $phone_numbers = [];

      $test_phone_number = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_phone_number');
      if (empty($test_phone_number)) {
        if ($this->statusMessage) {
          $this->drupalMessenger->addWarning($this->t('Test phone number is not set. Please set it in the SMSAPI settings.'));
        }
        $this->logger->warning('SMSAPI: Test phone number is not set. Please set it in the SMSAPI settings.');
        return NULL;
      }
      $sender = $this->config->get(self::SMSAPI_CONFIG)
        ->get('smsapi_test_sender_name');
      $sender = is_string($sender) ? $sender : '';
      $phone_numbers[] = $test_phone_number;
    }
    $service = $this->client->smsapiPlService($this->apiToken);
    $sms = SendSmssBag::withMessage($phone_numbers, $message);
    $sms->encoding = self::SMSAPI_ENCODING;
    if (!empty($sender)) {
      $sms->from = $sender;
    }
    if ($this->mockMode) {
      $sms->test = TRUE;
    }
    if ($this->partnerStatus) {
      $sms->partnerId = $this->partnerId;
    }

    try {
      $response = $service->smsFeature()->sendSmss($sms);
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Message sent'), 'status');
      }
      return $response;
    }
    catch (\Exception $e) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Failed to send multiple messages'), 'error');
      }
      $this->logger->error('SMSAPI: Error sending multiple messages @error', [
        '@error' => $e->getMessage(),
      ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSenders(): array {
    if ($this->checkIfConnected() === FALSE) {
      return ['Test' => 'Test'];
    }
    $service = $this->client->smsapiPlService($this->apiToken);
    $senders = $service->smsFeature()->sendernameFeature()->findSendernames();
    $active_senders = [];
    foreach ($senders as $sender) {
      if ($sender->status === self::SMSAPI_ACTIVE_SENDER) {
        $active_senders[$sender->sender] = $sender->sender;
      }
    }
    return $active_senders;
  }

  /**
   * {@inheritdoc}
   */
  public function getProfileData(): ?Profile {
    if ($this->checkIfConnected() === FALSE) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Missing or invalid API token. Please check your API token in the SMSAPI settings.'), 'error');
      }
      $this->logger->error('SMSAPI: Missing or invalid API token. Please check your API token in the SMSAPI settings.');
      return NULL;
    }
    $service = $this->client->smsapiPlService($this->apiToken);
    return $service->profileFeature()->findProfile();
  }

  /**
   * {@inheritdoc}
   */
  public function writeCallbackIntoLogs(Request $request): void {
    $msgId = $request->get('MsgId');
    $from = $request->get('from');
    $to = $request->get('to');
    $status = $request->get('status');
    $statusName = $request->get('status_name');

    if (
      is_string($msgId) &&
      is_string($from) &&
      is_string($to) &&
      is_string($status) &&
      is_string($statusName)
    ) {
      $msgId = explode(",", $msgId);
      $from = explode(",", $from);
      $to = explode(",", $to);
      $status = explode(",", $status);
      $statusName = explode(",", $statusName);

      for ($i = 0; $i < count($msgId); $i++) {
        $this->logger->info('Callback from SMSAPI: Message id: @MsgId from @from to @to have a status: [@status] @status_name', [
          '@MsgId' => $msgId[$i] ?? '',
          '@from' => $from[$i] ?? '',
          '@to' => $to[$i] ?? '',
          '@status' => $status[$i] ?? '',
          '@status_name' => $statusName[$i] ?? '',
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function sendVerificationCode(
      string $phone_number,
      string $template_name = '',
      array $values = [],
      string $sender = '',
  ): ?Mfa {
    $service = $this->client->smsapiPlService($this->apiToken);
    $this->mfaBag->phoneNumber = $phone_number;
    if (!empty($sender)) {
      $this->mfaBag->from = $sender;
    }
    if ($this->mockMode) {
      $this->mfaBag->fast = FALSE;
    }
    $message = (string) $this->t('Your verification code: [%code%]');
    if (!empty($template_name)) {
      $template = $this->smsapiSmsTemplateService->getTemplate($template_name);
      if ($template === NULL) {
        $this->logger->error('Template @template_name not found.', [
          '@template_name' => $template_name,
        ]);
        return NULL;
      }
      if (!str_contains($template->getMessage(), '[%code%]')) {
        $this->logger->error('Template @template_name does not contain [%code%] token.', [
          '@template_name' => $template_name,
        ]);
        return NULL;
      }
      $message = $this->smsapiSmsTemplateService->renderTemplate($template, $values);
    }
    $this->mfaBag->content = $message;
    try {
      $response = $service->mfaFeature()->generateMfa($this->mfaBag);

      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Message sent'), 'status');
      }
      return $response;
    }
    catch (\Exception $e) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Failed to send message'), 'error');
      }
      $this->logger->error('SMSAPI: Error sending message @error', [
        '@error' => $e->getMessage(),
      ]);
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkVerificationCode(string $code, string $phone_number): array {
    try {
      (new SmsapiHttpClient())
        ->smsapiPlService($this->apiToken)
        ->mfaFeature()
        ->verifyMfa(new VerificationMfaBag($code, $phone_number));

      return [
        'status' => TRUE,
        'message' => 'valid',
      ];
    }
    catch (ApiErrorException $ex) {
      if ($this->statusMessage) {
        $this->drupalMessenger->addMessage($this->t('Error while checking a verification code.') . ' ' .
          $ex->getCode() . ' ' . $this->t('Message:') . ' ' . $ex->getMessage(), 'error');
      }

      $this->logger->error('SMSAPI: Error while checking a verification code. Code: ' . $ex->getCode() . ' Error: ' . $ex->getMessage());

      return [
        'status' => FALSE,
        'message' => $ex->getMessage(),
        'code' => $ex->getCode(),
      ];
    }
  }

}
