<?php

declare(strict_types=1);

namespace Drupal\smsapi\Services;

use Drupal\smsapi\SmsapiSmsTemplateInterface;

/**
 * SMSAPI SMS Template Entity Service Interface.
 */
interface SmsapiSmsTemplateServiceInterface {

  /**
   * Get the SMSAPI SMS Template entity.
   *
   * @param string $template_name
   *   The SMSAPI SMS Template name.
   *
   * @return \Drupal\smsapi\SmsapiSmsTemplateInterface|null
   *   The SMSAPI SMS Template entity or NULL if not found.
   */
  public function getTemplate(string $template_name): ?SmsapiSmsTemplateInterface;

  /**
   * Get all SMSAPI SMS Template entities.
   *
   * @return array|null
   *   The SMSAPI SMS Template entities or NULL if not found.
   */
  public function getTemplates(): ?array;

  /**
   * Render the SMSAPI SMS Template message with the given values.
   *
   * @param \Drupal\smsapi\SmsapiSmsTemplateInterface $template
   *   The SMSAPI SMS Template entity.
   * @param array $values
   *   Array of values for replace.
   *   Index -> Token name.
   *   Value -> Replacement value.
   *   Example ['@token1' => 'Username', '@token2' => 'Value', '@xyz' => 'abc'].
   *
   * @return string
   *   The rendered SMSAPI SMS Template message.
   */
  public function renderTemplate(SmsapiSmsTemplateInterface $template, array $values): string;

}
