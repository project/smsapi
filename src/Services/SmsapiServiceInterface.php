<?php

declare(strict_types=1);

namespace Drupal\smsapi\Services;

use Smsapi\Client\Feature\Mfa\Data\Mfa;
use Smsapi\Client\Feature\Profile\Data\Profile;
use Smsapi\Client\Feature\Sms\Data\Sms;
use Symfony\Component\HttpFoundation\Request;

/**
 * SMSAPI Service Interface.
 */
interface SmsapiServiceInterface {

  /**
   * It checks if the user is connected to the SMSAPI service.
   *
   * @param string $api_token
   *   The SmsApi token.
   *
   * @return bool
   *   The result of the ping method.
   */
  public function checkIfConnected(string $api_token = ''): bool;

  /**
   * It sends an SMS message to a phone number using the SMSAPI.
   *
   * @param string $phone_number
   *   The phone number to send the SMS to.
   * @param string $message
   *   The message to send.
   * @param string $sender
   *   The sender name.
   *
   * @return \Smsapi\Client\Feature\Sms\Data\Sms|null
   *   The SMS object or NULL if the SMS was not sent.
   */
  public function sendSms(string $phone_number, string $message, string $sender = ''): ?Sms;

  /**
   * It sends an SMS message to a phone number using SMSAPI SMS Template and SMSAPI.
   *
   * @param string $phone_number
   *   The phone number to send the SMS to.
   * @param string $template_name
   *   The SMSAPI SMS Template name.
   * @param array $values
   *   The values to replace in the SMSAPI SMS Template.
   * @param string $sender
   *   The sender name.
   *
   * @return \Smsapi\Client\Feature\Sms\Data\Sms|null
   *   The SMS object or NULL if the SMS was not sent.
   */
  public function sendSmsWithTemplate(
      string $phone_number,
      string $template_name,
      array $values,
      string $sender = '',
  ): ?Sms;

  /**
   * It sends a SMSAPI SMS template message to a multiple phone numbers using the SMSAPI.
   *
   * @param array $phone_numbers
   *   The phone number to send the SMS to.
   * @param string $template_name
   *   The SMSAPI SMS Template name.
   * @param array $values
   *   The values to replace in the SMSAPI SMS Template.
   * @param string $sender
   *   The sender name.
   *
   * @return array|null
   *   The SMS objects array or NULL if the SMS was not sent.
   */
  public function sendMultipleTemplateSms(
      array $phone_numbers,
      string $template_name,
      array $values,
      string $sender = '',
  ): ?array;

  /**
   * It sends an SMS message to a multiple phone numbers using the SMSAPI.
   *
   * @param array $phone_numbers
   *   The phone number to send the SMS to.
   * @param string $message
   *   The message to send.
   * @param string $sender
   *   The sender name.
   *
   * @return array|null
   *   The SMS objects array or NULL if the SMS was not sent.
   */
  public function sendMultipleSms(array $phone_numbers, string $message, string $sender = ''): ?array;

  /**
   * It returns the SMSAPI defined senders.
   *
   * @return array
   *   List of senders.
   */
  public function getSenders(): array;

  /**
   * It returns the SMSAPI profile data.
   *
   * @return \Smsapi\Client\Feature\Profile\Data\Profile|null
   *   The profile data or NULL if the profile data was not fetched.
   */
  public function getProfileData(): ?Profile;

  /**
   * Writes single sms status from SMSAPI callback into logs.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object from SMSAPI.
   */
  public function writeCallbackIntoLogs(Request $request): void;

  /**
   * It sends an SMS message with a verification code.
   *
   * It's important that user defined template has a [%code%] token in it.
   *
   * @param string $phone_number
   *   The phone number to send the verification code to.
   * @param string $template_name
   *   The SMSAPI SMS Template name.
   * @param array $tokens
   *   The tokens value to replace in the SMSAPI SMS Template.
   * @param string $sender
   *   The sender name.
   *
   * @return \Smsapi\Client\Feature\Mfa\Data\Mfa|null
   *   The MFA object or NULL if the verification code sms was not sent.
   */
  public function sendVerificationCode(
      string $phone_number,
      string $template_name = '',
      array $tokens = [],
      string $sender = '',
  ): ?Mfa;

  /**
   * It checks the verification code in the API database.
   *
   * @param string $code
   *   The code which will be verified.
   * @param string $phone_number
   *   The phone number which will be verified.
   *
   * @return array
   *   Returns the associative array with 2 keys 'status' and 'message'
   *    'status' => FALSE,
   *    'message' => 'expired'
   *    ------------------------
   *    'status' => FALSE,
   *    'message' => 'not-valid'
   *    ------------------------
   *    'status' => FALSE,
   *    'message' => 'error'
   *    ------------------------
   *    'status' => TRUE,
   *    'message' => 'valid'
   */
  public function checkVerificationCode(string $code, string $phone_number): array;

}
