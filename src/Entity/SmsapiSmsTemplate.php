<?php

declare (strict_types=1);

namespace Drupal\smsapi\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\smsapi\SmsapiSmsTemplateInterface;

/**
 * Defines the SMSAPI SMS Template entity type.
 *
 * @ConfigEntityType(
 *   id = "smsapi_sms_template",
 *   label = @Translation("SMSAPI SMS Template"),
 *   label_collection = @Translation("SMSAPI SMS Templates"),
 *   label_singular = @Translation("SMSAPI SMS Template"),
 *   label_plural = @Translation("SMSAPI SMS Templates"),
 *   label_count = @PluralTranslation(
 *     singular = "@count SMSAPI SMS Template",
 *     plural = "@count SMSAPI SMS Templates",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\smsapi\SmsapiSmsTemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smsapi\Form\SmsapiSmsTemplateForm",
 *       "edit" = "Drupal\smsapi\Form\SmsapiSmsTemplateForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "smsapi_sms_template",
 *   admin_permission = "administer smsapi sms templates",
 *   links = {
 *     "collection" = "/admin/structure/smsapi-sms-template",
 *     "add-form" = "/admin/structure/smsapi-sms-template/add",
 *     "edit-form" = "/admin/structure/smsapi-sms-template/{sms_template}",
 *     "delete-form" = "/admin/structure/smsapi-sms-template/{sms_template}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "description" = "description",
 *     "message" = "message",
 *     "tokens" = "tokens",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "message",
 *     "tokens",
 *   },
 * )
 */
class SmsapiSmsTemplate extends ConfigEntityBase implements SmsapiSmsTemplateInterface {

  /**
   * The SMSAPI SMS Template ID.
   */
  protected string $id;

  /**
   * The SMSAPI SMS Template label.
   */
  protected string $label;

  /**
   * The SMSAPI SMS Template description.
   */
  protected string $description;

  /**
   * The SMSAPI SMS Template message.
   */
  protected string $message;

  /**
   * The SMSAPI SMS Template tokens.
   */
  protected string $tokens;

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokens(): array {
    $tokens = preg_split('/, |,/', $this->tokens);
    if (empty($tokens)) {
      return [];
    }
    return $tokens;
  }

}
