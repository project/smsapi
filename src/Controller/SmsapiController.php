<?php

declare (strict_types=1);

namespace Drupal\smsapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\smsapi\Services\SmsapiServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SmsapiController.
 *
 * Controller for handling all the SMSAPI requests.
 */
class SmsapiController extends ControllerBase {

  /**
   * SmsapiController constructor.
   *
   * @param \Drupal\smsapi\Services\SmsapiServiceInterface $smsapiService
   *   The SMSAPI Service.
   */
  public function __construct(private SmsapiServiceInterface $smsapiService) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('smsapi.service')
    );
  }

  /**
   * Shows profile page.
   *
   * @return array
   *   Render array.
   */
  public function profile(): array {
    return [
      '#theme' => 'smsapi__profile',
      '#profile_data' => $this->smsapiService->getProfileData(),
      '#attached' => [
        'library' => [
          'smsapi/profile',
        ],
      ],
    ];
  }

  /**
   * Returns the callback from SMSAPI.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns OK response to API. It prevents receiving duplicate reports.
   */
  public function createSmsLogs(Request $request): Response {
    if ($request->query->has('MsgId')) {
      $this->smsapiService->writeCallbackIntoLogs($request);
    }

    return new Response(
      'OK',
      Response::HTTP_OK
    );
  }

}
