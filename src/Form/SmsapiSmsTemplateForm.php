<?php

declare (strict_types=1);

namespace Drupal\smsapi\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * SMSAPI SMS Template form.
 *
 * @property \Drupal\smsapi\SmsapiSmsTemplateInterface $entity
 */
class SmsapiSmsTemplateForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the SMSAPI SMS Template.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\smsapi\Entity\SmsapiSmsTemplate::load',
        'source' => ['label'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the SMSAPI SMS Template.'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $this->entity->get('message'),
      '#description' => $this->t('Message of the SMSAPI SMS Template. To use tokens, enter a variable preceded by ! or @.
        Example:
        <em>This is a test message from @company</em>'),
      '#required' => TRUE,
    ];

    $form['tokens'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tokens'),
      '#default_value' => $this->entity->get('tokens'),
      '#description' => $this->t('Tokens of the SMSAPI SMS Template.
        Separate each of the tokens used with a comma.
        Example: <em>@company, @test, !username, !token4</em>'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new SMSAPI SMS Template %label.', $message_args)
      : $this->t('Updated SMSAPI SMS Template %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
