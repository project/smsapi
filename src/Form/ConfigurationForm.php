<?php

declare(strict_types=1);

namespace Drupal\smsapi\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smsapi\Services\SmsapiServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure SMSAPI settings for this site.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * Constructs a new SmsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\smsapi\Services\SmsapiServiceInterface $smsapiService
   *   The SMSAPI service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected SmsapiServiceInterface $smsapiService,
  ) {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('smsapi.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smsapi_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['smsapi.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['smsapi_environment_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the Test Environment'),
      '#description' => $this->t('Use the test environment instead of the production environment.'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_environment_status'),
    ];
    $form['smsapi_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMSAPI Token (OAuth)'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_token'),
    ];
    $form['smsapi_test_sender_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Test sender name (Only Test Environment)'),
      '#options' => $this->smsapiService->getSenders(),
      '#states' => [
        'visible' => [
          ':input[name="smsapi_environment_status"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_sender_name'),
    ];
    $form["smsapi_test_phone_number"] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test phone number (Only Test Environment)'),
      '#states' => [
        'visible' => [
          ':input[name="smsapi_environment_status"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_phone_number'),
    ];
    $form['smsapi_mock_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Mock SMS sending (Only Test Environment)'),
      '#states' => [
        'visible' => [
          ':input[name="smsapi_environment_status"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_mock_mode'),
    ];
    $form['smsapi_status_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show status message'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_status_message') ?? TRUE,
    ];
    $form['smsapi_partner'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use SMSAPI Partner ID'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_partner_status') ?? TRUE,
    ];
    $form['smsapi_partner_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMSAPI Partner ID'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_partner_id'),
      '#description' => $this->t('By using the code XLEJ when sending SMS messages, you support the developers of this module.'),
      '#states' => [
        'visible' => [
          ':input[name="smsapi_partner"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('smsapi_token') === '') {
      $form_state->setErrorByName('smsapi_token', (string) $this->t('The value is not correct.'));
    }

    $smsApiToken = $form_state->getValue('smsapi_token');
    if (is_string($smsApiToken) && !$this->smsapiService->checkIfConnected($smsApiToken)) {
      $form_state->setErrorByName('smsapi_token', (string) $this->t('Wrong token. Please check your token.'));
    }

    if ($form_state->getValue('smsapi_environment_status') === 1) {
      if ($form_state->getValue('smsapi_test_sender_name') === '') {
        $form_state->setErrorByName('smsapi_test_sender_name', (string) $this->t('The value is not correct.'));
      }
      if (
          $form_state->getValue('smsapi_test_phone_number') === ''
          || !is_numeric($form_state->getValue('smsapi_test_phone_number'))
      ) {
        $form_state->setErrorByName('smsapi_test_phone_number', (string) $this->t('The value is not correct.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('smsapi.settings')
      ->set('smsapi_token', $form_state->getValue('smsapi_token'))
      ->set('smsapi_environment_status', $form_state->getValue('smsapi_environment_status'))
      ->set('smsapi_test_sender_name', (bool) $form_state->getValue('smsapi_environment_status') ? $form_state->getValue('smsapi_test_sender_name') : '')
      ->set('smsapi_test_phone_number', (bool) $form_state->getValue('smsapi_environment_status') ? $form_state->getValue('smsapi_test_phone_number') : '')
      ->set('smsapi_mock_mode', (bool) $form_state->getValue('smsapi_environment_status') ? $form_state->getValue('smsapi_mock_mode') : FALSE)
      ->set('smsapi_status_message', $form_state->getValue('smsapi_status_message'))
      ->set('smsapi_partner', $form_state->getValue('smsapi_partner'))
      ->set('smsapi_partner_id', $form_state->getValue('smsapi_partner_id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
