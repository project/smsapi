<?php

declare(strict_types=1);

namespace Drupal\smsapi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smsapi\Services\SmsapiServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SMSAPI form.
 */
class SmsForm extends FormBase {

  /**
   * Constructs a new SmsForm object.
   *
   * @param \Drupal\smsapi\Services\SmsapiServiceInterface $smsapiService
   *   The SMSAPI Service.
   */
  public function __construct(protected SmsapiServiceInterface $smsapiService) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('smsapi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smsapi_sms';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['sender'] = [
      '#type' => 'select',
      '#title' => $this->t('Sender'),
      '#description' => $this->t('The sender of the message.'),
      '#options' => $this->smsapiService->getSenders(),
      '#required' => TRUE,
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_sender_name'),
    ];
    $form['recipient'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient'),
      '#description' => $this->t('The recipient of the message.'),
      '#placeholder' => $this->t('e.g. with Polish prefix 48123456789 or without 123456789'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_phone_number'),
      '#required' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The message to send.'),
      '#placeholder' => $this->t('e.g. Hello World!'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Send SMS'),
        '#button_type' => 'primary',
      ],
    ];

    if ($this->config('smsapi.settings')->get('smsapi_environment_status') === TRUE) {
      $form['recipient']['#disabled'] = TRUE;
      $form['sender']['#disabled'] = TRUE;
    }

    if (empty($this->config('smsapi.settings')->get('smsapi_token'))) {
      $form['token_info'] = [
        '#type' => 'markup',
        '#markup' => $this->t('To send the form, please provide your SMSAPI token.'),
        '#weight' => -1,
      ];

      $form['sender']['#disabled'] = TRUE;
      $form['recipient']['#disabled'] = TRUE;
      $form['message']['#disabled'] = TRUE;
      $form['actions']['#disabled'] = TRUE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('sender') == '') {
      $form_state->setErrorByName('sender', (string) $this->t('Sender is required.'));
    }
    if ($form_state->getValue('recipient') == '') {
      $form_state->setErrorByName('recipient', (string) $this->t('Recipient is required.'));
    }
    if ($form_state->getValue('recipient') != '' && !is_numeric($form_state->getValue('recipient'))) {
      $form_state->setErrorByName('recipient', (string) $this->t('Recipient must be a number.'));
    }
    if ($form_state->getValue('message') == '') {
      $form_state->setErrorByName('message', (string) $this->t('Message is required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $recipient = $form_state->getValue('recipient');
    $message = $form_state->getValue('message');
    $sender = $form_state->getValue('sender');

    if (
      is_string($recipient) &&
      is_string($message) &&
      is_string($sender)
    ) {
      $this->smsapiService->sendSms(
        strval($recipient),
        strval($message),
        strval($sender)
      );
    }
  }

}
