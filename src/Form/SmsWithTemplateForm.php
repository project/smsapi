<?php

declare (strict_types=1);

namespace Drupal\smsapi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\smsapi\Services\SmsapiServiceInterface;
use Drupal\smsapi\Services\SmsapiSmsTemplateServiceInterface;
use Drupal\smsapi\SmsapiSmsTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SMSAPI form.
 */
class SmsWithTemplateForm extends FormBase {

  /**
   * Constructs a new SmsForm object.
   *
   * @param \Drupal\smsapi\Services\SmsapiServiceInterface $smsapiService
   *   The SMSAPI Service.
   * @param \Drupal\smsapi\Services\SmsapiSmsTemplateServiceInterface $smsapiSmsTemplateService
   *   The SMSAPI SMS Template service.
   */
  public function __construct(
    protected SmsapiServiceInterface $smsapiService,
    protected SmsapiSmsTemplateServiceInterface $smsapiSmsTemplateService,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('smsapi.service'),
      $container->get('smsapi.template_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smsapi_sms_with_template';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['sender'] = [
      '#type' => 'select',
      '#title' => $this->t('Sender'),
      '#description' => $this->t('The sender of the message.'),
      '#options' => $this->smsapiService->getSenders(),
      '#required' => TRUE,
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_sender_name'),
    ];

    $form['recipient'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recipient'),
      '#description' => $this->t('The recipient of the message.'),
      '#placeholder' => $this->t('e.g. with Polish prefix 48123456789 or without 123456789'),
      '#default_value' => $this->config('smsapi.settings')->get('smsapi_test_phone_number'),
      '#required' => TRUE,
    ];

    $form['templates'] = [
      '#type' => 'select',
      '#title' => $this->t('Templates'),
      '#options' => $this->getTemplateOptions(),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#ajax' => [
        'callback' => '::loadTokenFields',
        'event' => 'change',
        'wrapper' => 'token-values-wrapper',
      ],
    ];
    $form['#tree'] = TRUE;
    $form['token_values'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'token-values-wrapper',
      ],
    ];

    $template = $form_state->getValue('templates');
    $template = is_string($template) ? $template : '';
    if (!empty($template)) {
      if ($form_state->getValue('templates') != $this->t('- Select -')) {
        $template_entity = $this->smsapiSmsTemplateService->getTemplate($template);
        if ($template_entity instanceof SmsapiSmsTemplateInterface) {
          $tokens = $template_entity->getTokens();
          $form['token_values']['sms-message'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Message'),
            '#value' => $template_entity->getMessage(),
            '#disabled' => TRUE,
          ];
          foreach ($tokens as $token) {
            $form['token_values'][$token] = [
              '#type' => 'textfield',
              '#title' => $this->t('Value for token: @token', [
                '@token' => $token,
              ]),
              '#values' => '',
            ];
          }
        }
      }
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send SMS'),
    ];

    if ($this->config('smsapi.settings')->get('smsapi_environment_status') === TRUE) {
      $form['recipient']['#disabled'] = TRUE;
      $form['sender']['#disabled'] = TRUE;
    }

    if (empty($this->config('smsapi.settings')->get('smsapi_token'))) {
      $form['token-info'] = [
        '#type' => 'markup',
        '#markup' => $this->t('To send the form, please provide your SMSAPI token.'),
        '#weight' => -1,
      ];

      $form['sender']['#disabled'] = TRUE;
      $form['recipient']['#disabled'] = TRUE;
      $form['templates']['#disabled'] = TRUE;
      $form['actions']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * Ajax callback to load token fields.
   */
  public function loadTokenFields(array $form, FormStateInterface $form_state): array {
    return $form['token_values'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('sender') == '') {
      $form_state->setErrorByName('sender', (string) $this->t('Sender is required.'));
    }
    if ($form_state->getValue('recipient') == '') {
      $form_state->setErrorByName('recipient', (string) $this->t('Recipient is required.'));
    }
    if ($form_state->getValue('recipient') != '' && !is_numeric($form_state->getValue('recipient'))) {
      $form_state->setErrorByName('recipient', (string) $this->t('Recipient must be a number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $phone_number = strval($form_state->getValue('recipient'));
    $sender = strval($form_state->getValue('sender'));
    $template = strval($form_state->getValue('templates'));
    $token_values = (array) $form_state->getValue('token_values');

    // Remove message field from array of tokens.
    unset($token_values['sms-message']);

    $this->smsapiService->sendSmsWithTemplate($phone_number, $template, $token_values, $sender);
  }

  /**
   * Load template names for form selector.
   */
  public function getTemplateOptions(): array {
    $options = [];
    $templates = $this->smsapiSmsTemplateService->getTemplates();
    if (is_array($templates)) {
      foreach ($templates as $template) {
        $options[$template->getId()] = $template->getId();
      }
    }
    return $options;
  }

}
