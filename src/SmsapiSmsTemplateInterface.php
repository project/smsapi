<?php

namespace Drupal\smsapi;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a SMSAPI SMS Template entity type.
 */
interface SmsapiSmsTemplateInterface extends ConfigEntityInterface {

  /**
   * Get the SMSAPI SMS Template id.
   *
   * @return string
   *   The SMSAPI SMS Template id.
   */
  public function getId(): string;

  /**
   * Get the SMSAPI SMS Template message.
   *
   * @return string
   *   The SMSAPI SMS Template message.
   */
  public function getMessage(): string;

  /**
   * Get the SMSAPI SMS Template tokens.
   *
   * @return array
   *   The SMSAPI SMS Template tokens.
   */
  public function getTokens(): array;

}
