# SMSAPI

## Contents of this file

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## Introduction

The sms module allows you to use the api provided by [SMSAPI](https://www.smsapi.pl/).
The module itself uses the library for php provided by api, but offers a Drupal
interface and some ready-made functions for programmers. This module is currently
available for Drupal 10.x.x.

The module interface provides a configuration form for connection, selection of
the test / production environment, sending a test sms, or even it provides
generating your own templates.

For programmers, there are ready-made functions that enable sending / verification
of mfa codes and receiving callbacks with the message status.

 * For the new ideas for the module visit:
   <link>

 * To submit bug reports and feature suggestions, or to track changes visit:
   [Issues](https://drupal.org/project/issues/smsapi)

## Requirements

This module requires SMSAPI library provided by [SMSAPI PHP Library](https://github.com/smsapi/smsapi-php-client).

## Installation

Install the SMSAPI module as you would normally install a contributed
Drupal module.

## Configuration

    1. Navigate to Administration > Extend
       and enable the module.
    2. Navigate to Administration > Configuration > System > SMSAPI
       for configurations. Save Configurations.

### Configurable parameters:

 * Use a Test Environment - you can switch between Production (set by default)
   or Test Environment.
 * SMSAPI Token (OAuth) - SMSAPI Token that you can generate on SMSAPI site
 * Test sender name (Only Test Environment) - you can create sender names on SMSAPI site.
   Name will be automatically in this configuration
 * Test phone number (Only Test Environment) - provides the functionality that sms will always
   be sent to this phone number
 * Mock SMS sending (Only Test Environment) - for test purpose. It will send the request to SMSAPI
   to send the sms, but actually it won't send anything. This is tool for testing if your connection
   with api is correct.
 * Show status message - shows the message status messages visible on the page.

To send a single / multiple sms message(s) with / without template,
follow the steps below:

  1. (Only for sms with template) Create a new sms message template by going to
    the /admin/structure/smsapi-sms-template subpage and press the Add SMS template button.
  2. Implement SmsapiService inside your class / form / block / etc.
  3. Use method:
    - sendSms() -> for sending single message
    - sendMultipleSms() -> for sending message to multiple phone numbers
    - sendSmsWithTemplate() -> for sending sms message using predefined template

### Two-Step Verification:

To use two-step verification (mfa) it is required to implement the service from SmsapiService.
The sendVerificationCode() method is responsible for sending the verification code, in which we provide two parameters,
phone number and sender name. To verify the codes in the SMSAPI, use the checkVerificationCode() method,
in which we also provide the phone number and code.

Callback reports must be sent to the <site-name>/smsapi/callback. To handle this, set on the SMSAPI website under
the address [Callbacks Dashboard](https://smsapi.pl/react/callbacks) the sms reports option and the url given above.

## Supporting organization:

 * Smartbees - https://www.drupal.org/smartbees
